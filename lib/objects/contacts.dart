import 'dart:convert';

class Contacts {
  String name;
  String email;
  String phone;
  bool isFavourite;
  Contacts({this.name, this.email, this.phone, this.isFavourite});

  factory Contacts.fromJson(Map<String, dynamic> jsonData) {
    return Contacts(
        name: jsonData['name'],
        email: jsonData['email'],
        phone: jsonData['phone'],
        isFavourite: jsonData['isFavourite']);
  }

  static Map<String, dynamic> toMap(Contacts contacts) => {
        'name': contacts.name,
        'email': contacts.email,
        'phone': contacts.phone,
        'isFavourite': contacts.isFavourite
      };

  static String encode(List<Contacts> musics) => json.encode(
        musics
            .map<Map<String, dynamic>>((music) => Contacts.toMap(music))
            .toList(),
      );

  static List<Contacts> decode(String musics) =>
      (json.decode(musics) as List<dynamic>)
          .map<Contacts>((item) => Contacts.fromJson(item))
          .toList();
}
