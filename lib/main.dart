import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:taskapp/constants/consts.dart';
import 'package:taskapp/screens/home.dart';
import 'package:taskapp/screens/showscreen.dart';
import 'dart:async';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.purple,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SplashScreen(
        seconds: 6,
        backgroundColor: Colors.purple,
        navigateAfterSeconds: HomeBar(),
        image: new Image.asset('images/contact.png'),
        photoSize: 120.0,
        loadingText: Text(
          "Welcome",
          style: TextStyle(color: Colors.white, fontSize: 25.0),
        ),
        loaderColor: Colors.white,
      ),
    );
  }
}

class HomeBar extends StatefulWidget {
  @override
  _HomeBarState createState() => _HomeBarState();
}

int n = 0;

class _HomeBarState extends State<HomeBar> {
  @override
  void initState() {
    // TODO: implement initState
    Timer.periodic((Duration(milliseconds: 1)), (timer) {
      setState(() {
        n = nameList.length;
      });
      print(n);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Contacts"),
          bottom: TabBar(
            tabs: [
              Tab(icon: Icon(Icons.person_add_alt_1), text: "Add Contact"),
              Tab(
                  icon: Icon(Icons.person_pin),
                  text: n == 0 ? "All Contacts" : "All Contacts (${n})")
            ],
          ),
        ),
        body: TabBarView(
          children: [
            MyHomePage(),
            PersonScreen(),
          ],
        ),
      ),
    );
  }
}
