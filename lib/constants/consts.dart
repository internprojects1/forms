import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

var nameList = List();
var emailList = List();
var phoneList = List();

void showToast(msgg) {
  Fluttertoast.showToast(
      msg: msgg,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.grey,
      textColor: Colors.white,
      fontSize: 16.0);
}
