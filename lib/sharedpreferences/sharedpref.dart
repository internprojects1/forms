import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class SharedPref {
  String storeKey = "Contacts";

  read() async {
    final prefs = await SharedPreferences.getInstance();
    return json.decode(prefs.getString(storeKey));
  }

  save(value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(storeKey, json.encode(value));
  }

  remove() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(storeKey);
  }
}
