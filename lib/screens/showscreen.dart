import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:taskapp/constants/consts.dart';

class PersonScreen extends StatefulWidget {
  @override
  _PersonScreenState createState() => _PersonScreenState();
}

class _PersonScreenState extends State<PersonScreen> {
  void removeTime(index) {
    String deletdName, deletedEmail, deletedPhone;
    setState(() {
      deletdName = nameList.removeAt(index);
      deletedEmail = emailList.removeAt(index);
      deletedPhone = phoneList.removeAt(index);
    });
    snackBar(deletdName, deletedEmail, deletedPhone, index);
  }

  void snackBar(deletedName, deletedEmail, deletedPhone, index) {
    Scaffold.of(context).showSnackBar(
      new SnackBar(
        content: new Text("Contact deleted"),
        action: SnackBarAction(
          label: "UNDO",
          onPressed: () {
            setState(() {
              nameList.insert(index, deletedName);
              emailList.insert(index, deletedEmail);
              phoneList.insert(index, deletedPhone);
            });
          },
        ),
      ),
    );
  }

  void showEditBox(index) async {
    TextEditingController nameEditController = new TextEditingController();
    TextEditingController emailEditController = new TextEditingController();
    TextEditingController phoneEditController = new TextEditingController();
    String editedName, editedEmail, editedPhone, msg = "";
    final _formEditKey = GlobalKey<FormState>();
    await showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: Form(
            key: _formEditKey,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(
                  height: 20.0,
                ),
                TextFormField(
                  controller: nameEditController,
                  keyboardType: TextInputType.name,
                  textAlign: TextAlign.left,
                  validator: (text) => text.isEmpty ? "Enter valid name" : null,
                  onChanged: (value) {
                    editedName = value;
                  },
                  decoration: InputDecoration(
                    hintText: "Enter your Name",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(2.0)),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                TextFormField(
                  controller: emailEditController,
                  keyboardType: TextInputType.emailAddress,
                  textAlign: TextAlign.left,
                  validator: (text) => text.isEmpty || !text.contains("@")
                      ? "Enter valid mail"
                      : null,
                  onChanged: (value) {
                    editedEmail = value;
                  },
                  decoration: InputDecoration(
                    hintText: "Enter your Email",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(2.0)),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                TextFormField(
                  controller: phoneEditController,
                  keyboardType: TextInputType.phone,
                  textAlign: TextAlign.left,
                  validator: (text) => text.isEmpty || text.length != 10
                      ? "Enter valid password"
                      : null,
                  onChanged: (value) {
                    editedPhone = value;
                  },
                  decoration: InputDecoration(
                    hintText: "Enter your phone number",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(2.0)),
                    ),
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              color: Colors.red,
              textColor: Colors.white,
              child: Text('CANCEL'),
              onPressed: () {
                setState(() {
                  Navigator.pop(context);
                });
              },
            ),
            FlatButton(
              color: Colors.green,
              textColor: Colors.white,
              child: Text('OK'),
              onPressed: () {
                setState(() {
                  Navigator.pop(context);
                  FocusScopeNode currentFocus = FocusScope.of(context);

                  if (!currentFocus.hasPrimaryFocus) {
                    currentFocus.unfocus();
                  }
                  if (_formEditKey.currentState.validate()) {
                    setState(() {
                      nameList[index] = editedName;
                      emailList[index] = editedEmail;
                      phoneList[index] = editedPhone;
                    });
                    nameEditController.clear();
                    emailEditController.clear();
                    phoneEditController.clear();
                    msg = "Added to contacts";
                    showToast(msg);
                  }
                });
              },
            ),
          ],
        );
      },
    );
  }

  void showOPtionBox(index) async {
    await showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                ListTile(
                    leading: Icon(
                      Icons.edit,
                      size: 60.0,
                    ),
                    title: Text("Edit"),
                    onTap: () {
                      Navigator.pop(context);
                      showEditBox(index);
                    }),
                ListTile(
                    leading: Icon(
                      Icons.edit,
                      size: 60.0,
                    ),
                    title: Text("Delete"),
                    onTap: () {
                      Navigator.pop(context);
                      removeTime(index);
                    }),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Column(
          children: [
            SizedBox(
              height: 15.0,
            ),
            nameList.length == 0
                ? Container()
                : ListView.builder(
                    //scrollDirection: Axis.vertical,
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: nameList.length,
                    itemBuilder: (BuildContext ctxt, int index) {
                      return Padding(
                        padding:
                            const EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                        child: Card(
                          elevation: 10.0,
                          child: Slidable(
                            actionPane: SlidableDrawerActionPane(),
                            actionExtentRatio: 0.25,
                            child: ListTile(
                              leading: Icon(
                                Icons.person_pin,
                                size: 60.0,
                              ),
                              title: Text(
                                  nameList[index] + "\n" + phoneList[index]),
                              subtitle: Text(emailList[index]),
                              onLongPress: () => showOPtionBox(index),
                            ),
                            actions: <Widget>[
                              IconSlideAction(
                                foregroundColor: Colors.white,
                                caption: 'Edit',
                                color: Colors.indigo,
                                icon: Icons.edit,
                                onTap: () => showEditBox(index),
                              ),
                            ],
                            secondaryActions: <Widget>[
                              IconSlideAction(
                                caption: 'Delete',
                                color: Colors.red,
                                icon: Icons.delete_outline,
                                onTap: () => removeTime(index),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
          ],
        ),
      ),
    );
  }
}

class _SystemPadding extends StatelessWidget {
  final Widget child;

  _SystemPadding({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    return new AnimatedContainer(
        padding: mediaQuery.viewInsets,
        duration: const Duration(milliseconds: 300),
        child: child);
  }
}

/*
FocusScopeNode currentFocus = FocusScope.of(context);

                if (!currentFocus.hasPrimaryFocus) {
                  currentFocus.unfocus();
                }
                if (_formEditKey.currentState.validate()) {
                  setState(() {
                    nameList[index] = editedName;
                    emailList[index] = editedEmail;
                    phoneList[index] = editedPhone;
                  });
                  nameEditController.clear();
                  emailEditController.clear();
                  phoneEditController.clear();
                  msg = "Added to contacts";
                  showToast(msg);
                }
*/
