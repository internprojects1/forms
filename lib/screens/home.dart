import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:taskapp/constants/consts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:taskapp/objects/contacts.dart';
import 'package:taskapp/sharedpreferences/sharedpref.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _formKey = GlobalKey<FormState>();
  String name, email, phone;
  final nameCont = TextEditingController();
  final emailCont = TextEditingController();
  final phoneCont = TextEditingController();
  String msg = "";

  SharedPref sharedPref = SharedPref();
  Contacts conSave = Contacts();
  Contacts conLoad = Contacts();
  List<Contacts> listContacts = List();
  loadSharedPrefs() async {
    try {
      Contacts user = Contacts.fromJson(await sharedPref.read());
      Scaffold.of(context).showSnackBar(SnackBar(
          content: new Text("Loaded!"),
          duration: const Duration(milliseconds: 500)));
      setState(() {
        conLoad = user;
        print(conLoad.name[0]);
      });
    } catch (Excepetion) {
      Scaffold.of(context).showSnackBar(SnackBar(
          content: new Text("Nothing found!"),
          duration: const Duration(milliseconds: 500)));
    }
  }

  // void showToast(msgg) {
  //   Fluttertoast.showToast(
  //       msg: msgg,
  //       toastLength: Toast.LENGTH_SHORT,
  //       gravity: ToastGravity.BOTTOM,
  //       timeInSecForIosWeb: 1,
  //       backgroundColor: Colors.grey,
  //       textColor: Colors.white,
  //       fontSize: 16.0);
  // }

  void setContactList(setName, setEmail, setPhone, setFavourite) {
    final String encodedData = Contacts.encode([
      Contacts(
          name: setName,
          email: setEmail,
          phone: setPhone,
          isFavourite: setFavourite)
    ]);
    print("encoded data   --->   $encodedData");
    final List<Contacts> decodedData = Contacts.decode(encodedData);
    print("decoded data   --->   ${decodedData.length}");
    print(decodedData[0]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //backgroundColor: Colors.grey[600],
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Form(
                key: _formKey,
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.all(18.0),
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 20.0,
                        ),
                        TextFormField(
                          controller: nameCont,
                          keyboardType: TextInputType.name,
                          textAlign: TextAlign.left,
                          validator: (text) =>
                              text.isEmpty ? "Enter valid name" : null,
                          onChanged: (value) {
                            conSave.name = value;
                            name = value;
                          },
                          decoration: InputDecoration(
                            hintText: "Enter your Name",
                            border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(2.0)),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        TextFormField(
                          controller: emailCont,
                          keyboardType: TextInputType.emailAddress,
                          textAlign: TextAlign.left,
                          validator: (text) =>
                              text.isEmpty || !text.contains("@")
                                  ? "Enter valid mail"
                                  : null,
                          onChanged: (value) {
                            conSave.email = value;
                            email = value;
                          },
                          decoration: InputDecoration(
                            hintText: "Enter your Email",
                            border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(2.0)),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        TextFormField(
                          controller: phoneCont,
                          keyboardType: TextInputType.phone,
                          textAlign: TextAlign.left,
                          validator: (text) => text.isEmpty || text.length != 10
                              ? "Enter valid password"
                              : null,
                          onChanged: (value) {
                            conSave.phone = value;
                            phone = value;
                          },
                          decoration: InputDecoration(
                            hintText: "Enter your phone number",
                            border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(2.0)),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        Material(
                          elevation: 5,
                          color: Colors.purple,
                          borderRadius: BorderRadius.circular(2.0),
                          child: MaterialButton(
                            onPressed: () {
                              FocusScopeNode currentFocus =
                                  FocusScope.of(context);

                              if (!currentFocus.hasPrimaryFocus) {
                                currentFocus.unfocus();
                              }
                              if (phoneList.contains(phone)) {
                                msg = "Already exists";
                                showToast(msg);
                              } else if (_formKey.currentState.validate()) {
                                setState(() {
                                  nameList.add(name);
                                  emailList.add(email);
                                  phoneList.add(phone);
                                  setContactList(name, email, phone, false);
                                  //n = nameList.length;
                                });
                                nameCont.clear();
                                emailCont.clear();
                                phoneCont.clear();
                                //sharedPref.save(storeKey, conSave);
                                //loadSharedPrefs();
                                msg = "Added to contacts";
                                showToast(msg);
                              }
                            },
                            minWidth: 190.0,
                            height: 45.0,
                            child: Text(
                              "Add contact",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 20.0),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
